// Initialize and add the map
function initMap() {}

$(document).ready(function() {
    // The location of Uluru
    console.log(LATITIDE + " " + LONGITUDE)
    const sortiePos = {lat: +LATITIDE, lng: +LONGITUDE};
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: sortiePos,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: sortiePos,
        map: map,
    });
});