
$(document).ready(function () {
    loadMap();
    $('#table thead tr').clone(true).appendTo('#table thead');
    var table = $("#table").DataTable({
        "stateSave": true,
        "paging": true,
        "ordering": false,
        "oLanguage": {
            "sEmptyTable": "Aucune sortie",
            "sSearch": "<span class=\"badge badge-light\">Le nom de la sortie contient :</span> ",
            "sZeroRecords": "Aucune sortie ",
            "oPaginate": {
                "sNext": "" + "<span class=\"badge badge-light\">Suivant</span>",
                "sPrevious": "<span class=\"badge badge-light\">  Précédent  </span>"
            }
        },
        initComplete: function (settings, json) {
            this.api().columns().every(function () {
                var column = this;
                var columnName = $(column.header()).data("name");
                var columnSize = $(column.header()).data("sizeSelect");
                if (columnName != null && ["Site", "Etat"].includes(columnName)) {
                    var select = $('<select style="width:' +
                        columnSize +
                        'px;" class="columnFilter"><option value="">' +
                        columnName +
                        '</option></select>')
                        .appendTo($(column.header()).empty())
                        .on('change',
                            function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            })
                        .on("click",
                            function (event) {
                                event.stopImmediatePropagation(); // pour ne pas trier la colonne à chaque clic sur le select
                            });
                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                    column.draw();
                }
            });

        }
    });
    $('#table thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        if (["Nom", "Inscrits/Places", "Organisateur"].includes(title)) {
            $(this).html('<input type="text" placeholder="' + title + '" />');
        }
        else if (title === "Date sortie" || title == "Clôture") {
            $(this).html('<input type="text" placeholder="' + title + '" />');
        }
        else if (["Description", "Actions"].includes(title)) {
            $(this).html("");
        }

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                    .column(i)
                    .search(this.value)
                    .draw();
            }
        });
    });
    var btnRecherche = $('#btnRecherche');
    btnRecherche.click(function () {
        console.log("dans recherhce");
        //on récupère tous les filtres
        var dateDebut = $('#dateDebut').val();
        var dateCloture = $('#dateCloture').val();
        var mesSorties = $('#sortiesOrganizeByMe').prop("checked");
        var sortiesInscrites = $('#sortiesISub').prop("checked");
        var sortiesPasInscrites = $('#sortiesDontSub').prop("checked");
        var sortiesPassees = $('#sortiesPassed').prop("checked");
        var sortiesArchived = $('#sortiesArchived').prop("checked");

        if ((dateDebut != null && dateDebut !== "") && (dateCloture != null && dateCloture !== "")) {
            if (dateDebut <= dateCloture) {
                window.location.href = ROOL_URL+"dateDebut=" + dateDebut + '/dateCloture=' + dateCloture + '/mesSorties=' + mesSorties + '/sortiesInscrites=' + sortiesInscrites + '/sortiesNI=' + sortiesPasInscrites + '/sortiesPassees=' + sortiesPassees;
            }
            else {
                return alert("La date de fin doit être supérieur à celle de début.");
            }
        }
        else {
            window.location.href = ROOL_URL
        }
        if ((dateDebut == null || dateDebut === "") && (dateCloture == null || dateCloture === "")) {
            window.location.href = ROOL_URL+"mesSorties=" + mesSorties + "/sortiesInscrites=" + sortiesInscrites + '/sortiesNI=' + sortiesPasInscrites + '/sortiesPassees=' + sortiesPassees;
        }
        if (sortiesArchived)
        {
            window.location.href = ROOL_URL+"SortiesArchived"
        }
    })
    //on interdit de cumuler les filtres "Sorties auxquelles je ne suis pas inscrit" et "Sorties auxquelles je suis inscrit"
    // ça revient au même d'afficher la page sans filtre

    //on restreint également l'accumulation de filtres lorsque "Sorties archivées" est coché
    var cbSortiesInscrit = $("#sortiesISub");
    var cbSortiesPasInscrit = $("#sortiesDontSub");
    var cbSortiesArchived = $("#sortiesArchived");
    var sortiesPassees = $('#sortiesPassed');
    var mesSorties = $('#sortiesOrganizeByMe');

    cbSortiesArchived.change(function (){
    if (cbSortiesArchived.prop("checked") === true){
        sortiesPassees.attr("disabled", true);
        cbSortiesInscrit.attr("disabled", true);
        cbSortiesPasInscrit.attr("disabled", true);
        mesSorties.attr("disabled", true);
        cbSortiesArchived.attr("disabled", false);
    }
    else {
        sortiesPassees.attr("disabled", false);
        cbSortiesInscrit.attr("disabled", false);
        cbSortiesPasInscrit.attr("disabled", false);
        mesSorties.attr("disabled", false);
        cbSortiesArchived.attr("disabled", false);
    }
});



    cbSortiesInscrit.change(function () {
        if (cbSortiesInscrit.prop("checked") === true) {
            cbSortiesPasInscrit.attr("disabled", true);
            cbSortiesInscrit.attr("disabled", false);

        }
        else {
            cbSortiesPasInscrit.attr("disabled", false);
            cbSortiesInscrit.attr("disabled", false);
        }
    });
    cbSortiesPasInscrit.change(function () {
        if (cbSortiesPasInscrit.prop("checked") === true) {
            cbSortiesPasInscrit.attr("disabled", false);
            cbSortiesInscrit.attr("disabled", true);

        }
        else {
            cbSortiesPasInscrit.attr("disabled", false);
            cbSortiesInscrit.attr("disabled", false);
        }
    });
});

// permet de s'inscrire à une sortie
function inscription(idSortie) {
    console.log("dans inscription");
    var user = USER_ID;
    var sortie = idSortie;
    $.ajax({
        url: ROOL_URL + "addInscription/" + user + "/" + sortie,
        success: function (data, textStatus, jqXHR) {
            document.location.reload();
        },
        complete: function (xhr, textStatus) {
            if (xhr.status === 304) {
                alert("La sortie est déjà passée, impossible de s'y inscrire!")
            };
        }
    });
}
//permet d'annuler une sortie
function annuler(idSortie) {
    var sortie = idSortie;
    console.log(idSortie);
    $.ajax({
        url: ROOL_URL + "cancelSortie/" + sortie,
        success: function (data, textStatus, jqXHR) {
            document.location.reload();
        },
        complete: function (xhr, textStatus) {
            if (xhr.status === 304) {
                alert("La sortie est déjà passée, impossible de s'y inscrire!")
            };
            if (xhr.status === 500) {
                alert("La sortie contient encore des élèves inscrits !")
            };
        }
    });
}
//permet de se désister d'une sortie
function desistement(idSortie) {
    var user = USER_ID;
    var sortie = idSortie;
    $.ajax({
        url: ROOL_URL + "deleteInscription/" + user + "/" + sortie,
        success: function (data, textStatus, jqXHR) {
            document.location.reload();
        },
        complete: function (xhr, textStatus) {
            if (xhr.status === 304) {
                alert("La sortie est déjà passée, impossible de s'y inscrire !")
            };
            if (xhr.status === 500) {
                alert("Vous n'êtes pas inscrit à cette sortie !")
            };
        }
    });
}
$(document).ready(function () {
    $("#table2").dataTable({
        "oLanguage": {
            "sEmptyTable": "Aucun participant",
            "sLengthMenu": "<span class=\"label label-default\">Liste des utilisateurs participants</span>",
            "sInfo": "",
            "sInfoFiltered": " <span class=\"label label-default\">Sorties filtrées sur un total de _MAX_ utilisateurs</span>",
            "sSearch": "<span class=\"label label-default\">Rechercher :</span> ",
            "sZeroRecords": "Aucune sortie ",
            "oPaginate": {
                "sNext": "<span class=\"label label-default\">Suivant</span>",
                "sPrevious": "<span class=\"label label-default\">Précédent</span>"
            }
        },
    });
});

/////////////
// MAP JS
/////////////


function loadMap() {
    var locations = COORD_LIST;
    console.log(locations)

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: new google.maps.LatLng(47.214166, -1.553878),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        console.log(locations[i][1])
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lng']),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(buildingMessage(locations[i]));
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
}
function buildingMessage(infos) {
    let contentString =
        '<link rel="stylesheet" href="bootstrap.min.js">' +
        '<h1 id="firstHeading" class="firstHeading">' + infos['lieu']['nom'] + '</h1>' +
        '<div id="bodyContent">' +
        '<div class="row justify-content-md-center">\n' +
        '    <div class="col-md-auto">\n' +
        '       <div class="row">\n' +
        '          <div class"col-md-auto">' +
        "              <label> Nom : " + infos['lieu']['nom'] +
        '          </div>' +
        '       </div>' +
        '       <div class="row">' +
        '          <div class"col-md-auto">' +
        '             <label>Adresse : ' + infos['lieu']['rue'] + '</label>' +
        '          </div>' +
        '        </div>' +
        '    </div>' +
        '    </div>' +
        '</div>' +
        '<ul class="list-group">'


    for (var i = 0; i < infos['sortie'].length; i++) {
        contentString +=
            '<a href="./afficherSortie/' + infos['sortie'][i]['noSortie'] + '" class="list-group-item list-group-item-action">\n' +
            '    <div class="d-flex w-100 justify-content-between">\n' +
            '      <h5 class="mb-1">' + infos['sortie'][i]['nomSortie'] + '</h5>\n' +
            '      <small><span class="badge badge-primary align-bottom">' + infos["sortie"][i]["Inscrit"] + "/" + infos["sortie"][i]["nbinscriptionsnmax"] + '</span></small>\n' +
            '      </div>' +
            '    <p class="mb-1">' + infos['sortie'][i]['description_info'] + '</p>' +
            '    <small></small>' +
            '  </a>'

    };
    for (let i = 0; i < infos.length; i++) {

    }
    contentString += "</ul></div>";
    return contentString;
}

