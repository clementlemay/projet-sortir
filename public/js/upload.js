$(document).ready(function() {

    //On écoute le "click" sur le bouton ayant la classe "modal-trigger"
    $('.valid-file').click(function () {

        console.log("Click")
        //On initialise les modales materialize
        $('.modal').modal();

        //On récupère l'url depuis la propriété "Data-target" de la balise html a
        file = document.getElementById('file').files[0];

        //on fait un appel ajax vers l'action symfony qui nous renvoie la vue
        $.ajax({
            url: "upload-users",
            type: "get", //send it through get method
            data: {
                file: file
            },
            success: function(response) {
                //Do Something
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
    })
});