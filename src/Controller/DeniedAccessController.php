<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeniedAccessController extends AbstractController
{
    /**
     * @Route("/denied-access", name="denied_access")
     */
    public function index()
    {
        return $this->render('denied_access/denied_access.html.twig', [
            'controller_name' => 'DeniedAccessController',
        ]);
    }
}
