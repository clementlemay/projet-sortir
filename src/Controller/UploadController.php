<?php

namespace App\Controller;

use App\Entity\Upload;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/upload", name="upload")
     */
    public function index(Request $request, KernelInterface $kernel)
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $file = $upload->getName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName($fileName);

            $application = new Application($kernel);
            $application->setAutoExit(false);

            $input = new ArrayInput([
                'command' => 'import:csv',
                'filename' => $this->getParameter('upload_directory').'/'.$upload->getName()
            ]);
            $output = new NullOutput();
            $application->run($input, $output);

            $this->addFlash('message', 'Utilisateurs importés avec succès');
            return $this->redirectToRoute('upload');
        }
        return $this->render('upload/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
