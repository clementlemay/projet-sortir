<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Entity\User;
use App\Form\ProfilFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ResetPasswordType;
use LogicException as LogicExceptionAlias;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         /*if ($this->getUser()) {
             return $this->redirectToRoute('target_path');
         }*/
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/profile", name="app_profil")
     */
    public function profil(AuthenticationUtils $authenticationUtils,Request $request): Response
    {
        $form = $this->createForm(ProfilFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            /** @var UploadedFile $pictureFile */
            $urlPhoto = $form->get('url_photo')->getData();

            if($urlPhoto !== null) {
                $newFilename = sha1(uniqid()) . "." . $urlPhoto->guessExtension();

                $urlPhoto->move($this->getParameter('upload_profil_dir'), $newFilename);

                $user->setUrlPhoto($newFilename);
            }
            // encode the plain password
            $user->setNom($form->get('nom')->getData());
            $user->setPrenom($form->get('prenom')->getData());
            $user->setTelephone($form->get('telephone')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setUsername($form->get('username')->getData());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
        }
        return $this->render('security/profil.html.twig', [
            'profilForm' => $form->createView(),
        ]);
    }

    /**
     * @param int $
     * @Route("/user/{id}", name="user_details", methods={"GET"})
     */
    public function userDetail(int $id, Request $request){

        $form = $this->createForm(ProfilFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // encode the plain password
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $user->setNom($form->get('nom')->getData());
            $user->setPrenom($form->get('prenom')->getData());
            $user->setTelephone($form->get('telephone')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setUsername($form->get('username')->getData());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        }
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        if(!$user){
            return $this->render('security/no_profil.html.twig', [
                'profilForm' => $form->createView(),
                'requestUser' => $user
            ]);
        }else{
            return $this->render('security/profil.html.twig', [
                'profilForm' => $form->createView(),
                'requestUser' => $user
            ]);
        }
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @param int $
     * @Route("/deleteUser/{id}", name="app_delete_user")
     */
    public function deleteUser(int $id, Request $request, UserRepository $userRepository,EntityManagerInterface $entityManager){
        $user = $userRepository->find($id);

        if(!$user){
            throw $this->createNotFoundException('No user found for id '.$id);
        } else {
            if($user->getIsAdministrateur() == false){
                $entityManager->remove($user);
                $entityManager->flush();

                $em = $this->getDoctrine()->getManager();
                $repo = $em->getRepository(Sortie::class);
                $sorties = $repo->listSorties();
                return $this->redirectToRoute('sortie');
            } else {
                // IMPOSSIBLE DE SUPPRIMER UN UTILISATEUR ADMIN
            }
        }
    }

    /**
     * @Route("/password-lost", name="app_forgotten_password")
     */
    public function forgottenPassword(Request $request, UserRepository $userRepo, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator){
        // On créer le formulaire
        $form = $this->createForm(ResetPasswordType::class);

        // On traite le formulaire
        $form->handleRequest($request);

        // Si le formulaire est valide
        if($form->isSubmitted() && $form->isValid()){
            // On récupère les données
            $donnees = $form->getData();

            // On cherche si un user a cet email
            $user = $userRepo->findOneBy(['email' => $donnees['email']]);

            // Si l'utilisateur existe pas
            if(!$user){
                // On envoi un message flash
                $this->addFlash('danger', "Cette adresse email n'existe pas");
                return $this->redirectToRoute('app_login');
            }

            // On génère un token
            $token = $tokenGenerator->generateToken();

            try {
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (\Exception $e){
                return $this->addFlash('warning', 'Une erreur est survenue : ' . $e->getMessage());
                return $this->redirectToRoute('app_login');
            }

            // On génère l'URL de réinitialisation de mot de passe
            $url = $this->generateUrl('app_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

            // On envoi le mail
            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('postmaster@sandbox1ea722943ec34c4f9e50dd852d3ba247.mailgun.org')
                ->setTo($user->getEmail())
                ->setBody(
                    "<p>Bonjour,</p> <br><br><p>Une demande de réinitialisation de mot de passe à été effectuée. Veuillez cliquer sur le lien suivant : " . $url . '</p>',
                    'text/html'
            );

            $mailer->send($message);
        }

        // On envoie vers la page de demande de l'email
        return $this->render('security/forgotten_password.html.twig', ['emailForm' => $form->createView()]);
    }

    /**
     * @Route("/reset-password/{token}", name="app_reset_password")
     */
    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder){
        // On cherche le user avec le token fournit
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['reset_token' => $token]);

        if(!$user){
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('app_login');
        }

        // Si le formulaire est envoyé en methode POST
        if($request->isMethod('POST')){
            // On supprime le token de l'utilisateur
            $user->setResetToken(null);

            //On chiffre le mot de passe
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('message', 'Mot de passe modifié avec succès');

            return $this->redirectToRoute('app_login');
        }else{
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new LogicExceptionAlias('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
