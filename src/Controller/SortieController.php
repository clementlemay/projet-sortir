<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Inscription;
use App\Entity\Lieu;
use App\Entity\User;
use App\Form\LieuType;
use App\Form\SortiesModifType;
use App\Entity\Ville;
use App\Form\SortieType;
use App\Repository\CampusRepository;
use App\Repository\EtatRepository;
use App\Repository\LieuRepository;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SortieController extends AbstractController
{
    /**
     * @Route("/sortie", name="sortie")
     * @Route("/dateDebut={dateDebut}/dateCloture={dateCloture}/mesSorties={mesSorties}/sortiesInscrites={sortiesInscrites}/sortiesNI={sortiesNI}/sortiesPassees={sortiesPassees}")
     * @Route("/mesSorties={mesSorties}/sortiesInscrites={sortiesInscrites}/sortiesNI={sortiesNI}/sortiesPassees={sortiesPassees}", name="mesSorties")
     * @param EntityManagerInterface $em
     * @param string $dateDebut
     * @param string $dateCloture
     * @param string $mesSorties
     * @param string|null $sortiesInscrites
     * @param string|null $sortiesNI
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(string $dateDebut = null, string $dateCloture = null, string $mesSorties = null, string $sortiesInscrites = null, string $sortiesNI = null,
                          string $sortiesPassees = null)
    {
        $user = $this->getUser();
        $idUser = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Sortie::class);
        $coordList = array();
        $alreadyAddCoord = array();

        $lieux = $em->getRepository(Lieu::class)->findAll();

        $sorties = $em->getRepository(Sortie::class)->listSortiesArchived();
        $alreadyAdded = false;
        foreach ($sorties as $s) {
            foreach ($lieux as $l) {
                foreach ($alreadyAddCoord as $a) {
                    if ($a == $l->getNoLieu()) {
                        $alreadyAdded = true;
                    }
                }
                if ($s['no_lieu_id'] == $l->getNoLieu() && $alreadyAdded == false) {
                    $coords = [
                        'sortie' => $em->getRepository(Sortie::class)->listSortiesByLieu($l->getNoLieu()),
                        'lieu' => [
                            'nom' => $l->getNomLieu(),
                            'rue' => $l->getRue(),
                        ],
                        'lat' => $l->getLatitude(),
                        'lng' => $l->getLongitude(),
                    ];
                    array_push($coordList, $coords);
                    array_push($alreadyAddCoord, $l->getNoLieu());
                }
                $alreadyAdded = false;
            }
        }
        $sites = $em->getRepository(Campus::class)->findAll();
        if ($dateDebut != null && $dateCloture != null) {
            $dateDebut = new \DateTime($dateDebut);
            $dateCloture = new \DateTime($dateCloture);
            // si la case à cocher "Mes sorties" n'est pas coché
            if ($mesSorties === "false") {
                // si la case à cocher "Sorties [..] je suis inscrit" est pas coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "false") {
                    //on filtre selon les dates
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture);
                }
                // si la case à cocher "Sorties [..] je suis inscrit" est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "false") {
                    // on filtre selon les dates et les sorties auxquelles l'user est inscrit
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 1);
                }
                // si la càc sorties non inscrites est coché
                if ($sortiesNI === "true" && $sortiesPassees === "false") {
                    // on filtre selon dates + càc
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 1);
                }
                // si la càc sorties passées est coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 0, 1);
                }
                // si la càc sorties passées + sorties inscrites est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 1, 0, 1);
                }
                // si la càc sorties passées + sorties ni est coché
                if ($sortiesNI === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 1, 1);
                }
            }
            //si la case à cocher "MesSorties" est coché
            if ($mesSorties === "true") {
                //si aucune càc n'est pas coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "false") {
                    //on filtre selon les dates et selon les sorties de l'utilisateur connecté
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 0, 0);
                }
                // si la càc sortiesinscrites est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 1, 0, 0);
                }
                // si la càc sorties ni est coché
                if ($sortiesNI === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 1, 0);
                }
                // si la càc sorties passées est coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 0, 1);
                }
                // si la càc sorties passées + sorties inscrites est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 1, 0, 1);
                }
                // si la càc sorties passées + sorties ni est coché
                if ($sortiesNI === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 1, 1);
                }
            }
        } else {

            //Chargement du datatable par défaut càd sans date ni filtre
            $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture);
            // la càc mesSorties est coché
            if ($mesSorties === "true") {
                // la càc sortiesinscrites et sorties non inscrites et sorties passé n'est pas coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 0, 0);
                }
                // la càc sortiesinscrites est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 1, 0, 0);
                }
                // la càc sortiesNI est coché
                if ($sortiesNI === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 1, 0);
                }
                // la càc sortiespasses est coché
                if ($sortiesInscrites === "false" && $sortiesNI === "false" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 0, 1);
                }
                // la càc sortiesinscrites + sortiespassees est coché
                if ($sortiesInscrites === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 1, 0, 1);
                }
                // la càc sortiesNI + + sortiespassees est coché
                if ($sortiesNI === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 1, 0, 1, 1);
                }
            }
            // si messorties n'est pas coché
            if ($mesSorties === "false") {
                // et que sorties inscrites l'est
                if ($sortiesInscrites === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 1, 0, 0);
                }
                // sorties inscrites + sorties passees
                if ($sortiesInscrites === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 1, 0, 1);
                }
                //sorties non inscrites
                if ($sortiesNI === "true" && $sortiesPassees === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 1, 0);
                }
                //sorties non inscrites + sorties passees
                if ($sortiesNI === "true" && $sortiesPassees === "true") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 1, 1);
                }
                // si sorties passes est coché
                if ($sortiesPassees === "true" && $sortiesInscrites === "false" && $sortiesNI === "false") {
                    $sorties = $repo->listSorties($idUser, $dateDebut, $dateCloture, 0, 0, 0, 1);
                }
            }

        }
        return $this->render('sortie/index.html.twig', ['sortie' => $sorties, "sites" => $sites, 'coords' => json_encode($coordList)]);
    }

    /**
     * @Route("/SortiesArchived", name="SortiesArchived")
     */
    public function sortiesArchived()
    {
        $user = $this->getUser();
        $idUser = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Sortie::class);
        $coordList = array();
        $alreadyAddCoord = array();
        $sites = $em->getRepository(Campus::class)->findAll();
        $lieux = $em->getRepository(Lieu::class)->findAll();
        $alreadyAdded = false;

        $sorties = $em->getRepository(Sortie::class)->listeSortiesArchived();
        foreach ($sorties as $s) {
            foreach ($lieux as $l) {
                foreach ($alreadyAddCoord as $a) {
                    if ($a == $l->getNoLieu()) {
                        $alreadyAdded = true;
                    }
                }
                if ($s['no_lieu_id'] == $l->getNoLieu() && $alreadyAdded == false) {
                    $coords = [
                        'sortie' => $em->getRepository(Sortie::class)->listSortiesByLieu($l->getNoLieu()),
                        'lieu' => [
                            'nom' => $l->getNomLieu(),
                            'rue' => $l->getRue(),
                        ],
                        'lat' => $l->getLatitude(),
                        'lng' => $l->getLongitude(),
                    ];
                    array_push($coordList, $coords);
                    array_push($alreadyAddCoord, $l->getNoLieu());
                }
                $alreadyAdded = false;
            }
        }


        return $this->render('sortie/index.html.twig', ['sortie' => $sorties, "sites" => $sites, 'coords' => json_encode($coordList)]);

    }

    /**
     * @Route("/ajout-sortie", name="ajout-sortie")
     */
    public function addSortie(Request $request,
                              EntityManagerInterface $em,
                              CampusRepository $campusRepository,
                              EtatRepository $etatRepository,
                              VilleRepository $villeRepository,
                              LieuRepository $lieuRepository,
                              UserRepository $userRepository
    )
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $organisateur = $this->getUser();

        $sortie = new Sortie();
        $sortieForm = $this->createForm(SortieType::class, $sortie);

        $sortieForm->handleRequest($request);

        if ($sortieForm->isSubmitted() && $sortieForm->isValid()) {
            /** @var UploadedFile $pictureFile */
            $urlPhoto = $sortieForm->get('url_photo')->getData();

            if ($urlPhoto !== null) {
                $newFilename = sha1(uniqid()) . "." . $urlPhoto->guessExtension();

                $urlPhoto->move($this->getParameter('upload_sortie_dir'), $newFilename);

                $sortie->setUrlPhoto($newFilename);
            }


            $sortie->setOrganisateur($organisateur);
            $sortie->setIsArchived(false);

            $em->persist($sortie);
            $em->flush();

            return $this->redirectToRoute('sortie');
        }

        //FORMULAIRE NOUVEAU LIEU
        $modalOnOff = false;

        $lieu = new Lieu();
        $lieuForm = $this->createForm(LieuType::class, $lieu);

        $lieuForm->handleRequest($request);

        /*if($lieuForm->isValid()) */
        if ($lieuForm->isSubmitted() && $lieuForm->isValid()) {

            $em->persist($lieu);
            $em->flush();
        } elseif ($lieuForm->isSubmitted() && !$lieuForm->isValid()) {
            $modalOnOff = true;
        }

        return $this->render('sortie/ajout-sortie.html.twig', [
            'controller_name' => 'SortieController',
            'sortieForm' => $sortieForm->createView(),
            'lieuForm' => $lieuForm->createView(),
            'modalOnOff' => $modalOnOff,
        ]);
    }
    /**
     * @Route("/cancelSortie/{id}", name="cancelSortie")
     * @param int $id
     * @return Response
     */
    public function cancelSortie(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $sortie = $em->getRepository(Sortie::class)->find($id);
        $etat = $em->getRepository(Etat::class)->findOneBy(['libelle' => 'Annulée']);
        $sortie->setNoEtat($etat);
        $sortie->setIsArchived(true);
        $em->persist($sortie);
        $em->flush();
        return new Response("ok", Response::HTTP_OK);
    }

    /**
     * @route("/afficherSortie/{id}", name="afficherSortie")
     * @param int $id
     * @param EntityManagerInterface $em
     * @return Response
     * @throws DBALException
     */
    public function afficherSortie(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $sortieList = $em->getRepository(Sortie::class);
        $siteList = $em->getRepository(Campus::class);
        $lieuList = $em->getRepository(Lieu::class);
        $villeList = $em->getRepository(Ville::class);
        $userList = $em->getRepository(User::class);
        $etatList = $em->getRepository(Etat::class);
        $inscriptionList = $em->getRepository(Inscription::class);
        $sortie = $sortieList->find($id);
        $site = $siteList->find($sortie->getNoSortie());
        $lieu = $lieuList->find($sortie->getNoLieu());
        $ville = $villeList->find($lieu->getNoVille());
        $etat = $etatList->find($sortie->getNoEtat());

        $inscriptions = $inscriptionList->findBy(['noSortie' => $id]);

        $users = [];

        foreach ($inscriptions as $ins) {
            $users[] = $userList->find($ins->getNoParticipant());
        }

        return $this->render('sortie/afficher.html.twig', [
            'sortieNo' => $sortie->getNoSortie(),
            'sortieNom' => $sortie->getNom(),
            'sortieDateDebut' => date_format($sortie->getDatedebut(), "Y/m/d H:i:s"),
            'sortieDateFin' => date_format($sortie->getDatecloture(), "Y/m/d H:i:s"),
            'sortieNbPlace' => $sortie->getNbInscriptionMax(),
            'sortieDuree' => $sortie->getDuree(),
            'sortieDescription' => $sortie->getDescriptionInfo(),
            'organisateur' => $sortie->getOrganisateur()->getId(),
            'etatLib' => $etat->getLibelle(),
            'siteNom' => $ville->getNomVille(),
            'lieuNom' => $lieu->getNomLieu(),
            'lieuRue' => $lieu->getRue(),
            'villeCp' => $ville->getCodePostal(),
            'latitude' => $lieu->getLatitude(),
            'longitude' => $lieu->getLongitude(),
            'userList' => $users,
            'sortieUrlPhoto' => $sortie->getUrlPhoto()
        ]);
    }

    /**
     * @Route("/modifSortie/{id}", name = "modif_sortie")
     * @param int $id
     */
    public function update(Request $request, $id = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $sortieLieu = $em->getRepository(Sortie::class);
        $sortie = $sortieLieu->find($id);
        $sortie->setNoCampus($this->getUser()->getNoCampus());
        $form = $this->createForm(SortiesModifType::class, $sortie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            echo($request);
            //$em->persist($sortie);
            $em->flush();

            $this->addFlash('success', 'Sortie modifié !');
            return $this->redirectToRoute('sortie');
        }
        return $this->render('sortie/update.html.twig', ["sortieModifForm" => $form->createView(), "sortie" => $sortie]);
    }

    /**
     * @route("/updateEtatSortie/{idSortie}/{libEtat}", name="update_etat_sortie")
     * @param int $idSortie
     * @param string $libEtat
     */
    public function updateEtatSortie(int $idSortie, string $libEtat)
    {
        $em = $this->getDoctrine()->getManager();
        $sortie = $em->getRepository(Sortie::class)->find($idSortie);
        $etat = $em->getRepository(Etat::class)->findOneBy(['libelle' => $libEtat]);
        if ($libEtat == 'Passée' || $libEtat == 'Annulée') {
            $sortie->setIsArchived(true);
        }
        $sortie->setNoEtat($etat);
        $em->persist($sortie);
        $em->flush();
        return $this->afficherSortie($idSortie);
    }
}
