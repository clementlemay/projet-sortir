<?php

namespace App\Controller;
use App\Entity\Sortie;
use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Inscription;
use App\Entity\Lieu;
use App\Entity\User;
use DateTime;
use Exception;
use App\Entity\Ville;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InscriptionController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function index()
    {
        return $this->render('inscription/index.html.twig', [
            'controller_name' => 'InscriptionController',
        ]);
    }


 /**
     * @Route("/addInscription/{userId}/{sortieId}", name="addInscription")
     * @return Response
     * @throws Exception
     */
    public function addInscription(User $userId, Sortie $sortieId)
    {
        $em = $this->getDoctrine()->getManager();
        if($sortieId->getDateCloture() < new DateTime('NOW'))
        {
            return new Response("merde",Response::HTTP_NOT_MODIFIED);
        }
        $inscription = new Inscription();
        $inscription->setNoSortie($sortieId);
        $inscription->setNoParticipant($userId);
        $inscription->setDateInscription(new DateTime('NOW'));
        $em->persist($inscription);
        $em->flush();
        return new Response("ok",Response::HTTP_OK);
    }

        /**
     * @Route("/deleteInscription/{userId}/{sortieId}", name="deleteInscription")
     * @param User $userId
     * @param Sortie $sortieId
     * @return Response
     */
    public function deleteInscription(User $userId, Sortie $sortieId)
    {
        $em = $this->getDoctrine()->getManager();
        $inscriptionList = $em->getRepository(Inscription::class);
        $inscription = $inscriptionList->findOneBy(["noParticipant"=>$userId->getId(),"noSortie"=>$sortieId->getNoSortie()]);
        $em->remove($inscription);
        $em->flush();
        return new Response("ok",Response::HTTP_OK);
    }
}
