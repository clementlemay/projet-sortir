<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LieuRepository::class)
 */
class Lieu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $noLieu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nomLieu;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $rue;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\ManyToOne(targetEntity=Ville::class)
     * @ORM\JoinColumn(referencedColumnName="no_ville", nullable=false)
     */
    private $noVille;


    public function getNoLieu(): ?int
    {
        return $this->noLieu;
    }

    public function setNoLieu(?Integer $noLieu): self
    {
        $this->noLieu = $noLieu;

        return $this;
    }

    public function getNomLieu(): ?string
    {
        return $this->nomLieu;
    }

    public function setNomLieu(?string $nomLieu): self
    {
        $this->nomLieu = $nomLieu;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(?string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getNoVille(): ?Ville
    {
        return $this->noVille;
    }

    public function setNoVille(?Ville $noVille): self
    {
        $this->noVille = $noVille;

        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('latitude', new Assert\Regex([
            'pattern' => '/\-?\b([0-9]|[1-9][0-9])\b\.\d\d\d\d\d\d/',
            'match' => true,
            'message' => 'La latitude doit correspondre à une valeur entre -90 et 90 degrés, un "." suivi de 6 décimales',
        ]));

        $metadata->addPropertyConstraint('longitude', new Assert\Regex([
            'pattern' => '/\-?\b([0-9]|[1-9][0-9]|1[0-8][0-9])\b\.\d\d\d\d\d\d/',
            'match' => true,
            'message' => 'La longitude doit correspondre à une valeur entre -180 et 180, un "." suivi de 6 décimales',
        ]));
    }
}
