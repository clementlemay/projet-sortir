<?php

namespace App\Entity;

use App\Repository\InscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 */
class Inscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $noId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateInscription;

    /**
     * @ORM\ManyToOne(targetEntity=Sortie::class)
     * @ORM\JoinColumn(referencedColumnName="no_sortie",nullable=false, onDelete="CASCADE")
     */
    private $noSortie;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id",nullable=false, onDelete="CASCADE")
     */
    private $noParticipant;

    /**
     * @return mixed
     */
    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    /**
     * @param mixed $dateInscription
     */
    public function setDateInscription($dateInscription): void
    {
        $this->dateInscription = $dateInscription;
    }

    public function getNoSortie(): ?Sortie
    {
        return $this->noSortie;
    }

    public function setNoSortie(?Sortie $noSortie): self
    {
        $this->noSortie = $noSortie;

        return $this;
    }

    public function getNoParticipant(): ?User
    {
        return $this->noParticipant;
    }

    public function setNoParticipant(?User $noParticipant): self
    {
        $this->noParticipant = $noParticipant;

        return $this;
    }
}
