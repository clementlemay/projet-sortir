<?php

namespace App\Entity;

use App\Repository\VilleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VilleRepository::class)
 */
class Ville
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $noVille;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nomVille;

    /**
     * @ORM\Column(type="integer")
     */
    private $codePostal;

    /**
     * @return mixed
     */
    public function getNoVille()
    {
        return $this->noVille;
    }

    /**
     * @param mixed $noVille
     */
    public function setNoVille($noVille): void
    {
        $this->noVille = $noVille;
    }

    /**
     * @return mixed
     */
    public function getNomVille()
    {
        return $this->nomVille;
    }

    /**
     * @param mixed $nomVille
     */
    public function setNomVille($nomVille): void
    {
        $this->nomVille = $nomVille;
    }

    /**
     * @return mixed
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     */
    public function setCodePostal($codePostal): void
    {
        $this->codePostal = $codePostal;
    }
}
