<?php

namespace App\Entity;

use App\Repository\CampusRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CampusRepository::class)
 */
class Campus
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $noCampus;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $nomCampus;

    /**
     * @return mixed
     */
    public function getNoCampus()
    {
        return $this->noCampus;
    }

    /**
     * @param mixed $noCampus
     */
    public function setNoCampus($noCampus): void
    {
        $this->noCampus = $noCampus;
    }

    /**
     * @return mixed
     */
    public function getNomCampus()
    {
        return $this->nomCampus;
    }

    /**
     * @param mixed $nomCampus
     */
    public function setNomCampus($nomCampus): void
    {
        $this->nomCampus = $nomCampus;
    }
}
