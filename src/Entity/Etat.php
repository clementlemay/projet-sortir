<?php

namespace App\Entity;

use App\Repository\EtatRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EtatRepository::class)
 */
class Etat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $noEtat;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $libelle;

    /**
     * @return mixed
     */
    public function getNoEtat()
    {
        return $this->noEtat;
    }

    /**
     * @param mixed $noEtat
     */
    public function setNoEtat($noEtat): void
    {
        $this->noEtat = $noEtat;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }
}
