<?php
// src/DataFixtures/FakerFixtures.php
namespace App\DataFixtures;

use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Inscription;
use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Entity\User;
use App\Entity\Ville;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;
use App\DataFixtures\Test;

class FakerFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $etatList = array("Créée", "Ouverte", "Clôturée", "Activé en cours", "Passée", "Annulée");
        $villeList = array("Nantes", "Niort", "Rennes", "Montréal");


        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 5; $i++) {
            $campus = new Campus();
            $campus->setNomCampus($faker->lastName);
            $manager->persist($campus);
        }

        foreach ($etatList as $e) {
            $etat = new Etat();
            $etat->setLibelle($e);
            $manager->persist($etat);
        }

        foreach ($villeList as $v) {
            $ville = new Ville();
            $ville->setNomVille($v);
            $ville->setCodePostal(12345);
            $manager->persist($ville);
        }
        $manager->flush();
        $allCampus = $manager->getRepository(Campus::class)->findAll();
        $allVilles = $manager->getRepository(Ville::class)->findAll();
        $allEtat = $manager->getRepository(Etat::class)->findAll();

        for ($i = 0; $i < 20; $i++) {
            $lieu = new Lieu();
            $ville = $faker->randomElement($allVilles);
            if ($ville->getNomVille() == 'Nantes'){
                $lieu->setLatitude($faker->latitude($min = 47.175537, $max = 47.255420));
                $lieu->setLongitude($faker->longitude($min = -1.603914, $max = -1.512077));
            } elseif($ville->getNomVille() == 'Niort') {
                $lieu->setLatitude($faker->latitude($min = 46.312739809799744, $max = 46.33119483273899));
                $lieu->setLongitude($faker->longitude($min = -0.45154125745223483, $max = -0.4754513744934563));
            } elseif($ville->getNomVille() == 'Rennes') {
                $lieu->setLatitude($faker->latitude($min = 48.09551128718734, $max = 48.130779087569685));
                $lieu->setLongitude($faker->longitude($min = -1.6419965769023603, $max = -1.703534109062883));
            } elseif($ville->getNomVille() == 'Montréal') {
                $lieu->setLatitude($faker->latitude($min = 45.439607, $max = 45.595053));
                $lieu->setLongitude($faker->longitude($min = -73.593676, $max = -73.808380));
            }
             $lieu->setNomLieu($faker->streetName);
            $lieu->setRue($faker->streetAddress);
            $lieu->setNoVille($ville);
            $manager->persist($lieu);
        }
        $manager->flush();
        $allLieux = $manager->getRepository(Lieu::class)->findAll();

        $this->createSpecificuUser($allCampus, $faker, $manager);
        for ($i = 0; $i < 100; $i++) {
            $user = new User();
            $user->setUsername($faker->userName);
            $user->setNom($faker->lastName);
            $user->setPrenom($faker->firstName($gender = null));
            $user->setTelephone($faker->phoneNumber);
            $user->setEmail($faker->freeEmail);
            $user->setPassword($faker->password);
            $user->setIsAdministrateur($faker->boolean);
            $user->setIsActif($faker->boolean);
            $user->setNoCampus($faker->randomElement($allCampus));
            $user->setUrlPhoto(null);
            if ($user->getIsAdministrateur()) {
                $user->setRoles(array("ROLE_ADMIN"));
            } else {
                $user->setRoles(array("ROLE_USER"));
            }
            $manager->persist($user);
        }
        $manager->flush();
        $allUser = $manager->getRepository(User::class)->findAll();

        for ($i = 0; $i < 50; $i++) {
            $sortie = new Sortie();
            $sortie->setNom($faker->domainWord);
            $sortie->setDateDebut($faker->dateTimeBetween($startDate = '-1 months', $endDate = '+ 2 months', $timezone = null));
            $sortie->setDuree($faker->numberBetween($min = 1, $max = 120));
            $sortie->setDateCloture($faker->dateTimeBetween($startDate = '-1 months', $endDate = $sortie->getDateDebut(), $timezone = null));
            $sortie->setNbInscriptionMax($faker->numberBetween($min = 30, $max = 99));
            $sortie->setDescriptionInfo($faker->text($maxNbChars = 200));
            $sortie->setUrlPhoto(null);
            $sortie->setOrganisateur($faker->randomElement($allUser));
            $sortie->setNoLieu($faker->randomElement($allLieux));
            $sortie->setNoCampus($faker->randomElement($allCampus));
            $sortie->setIsArchived(false);
            $restrictedEtatList = array();
            foreach ($allEtat as $etat) {
                if (strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s')) < strtotime(date('Y-m-d H:i:s'))) {
                    if ($etat->getLibelle() == "Passée" || $etat->getLibelle() == "Annulée") {
                        $sortie->setIsArchived(true);
                        array_push($restrictedEtatList, $etat);
                    }

                } else {
                    if (strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s')) > strtotime(date('Y-m-d H:i:s')) && strtotime($sortie->getDateCloture()->format('Y-m-d H:i:s')) >= strtotime(date('Y-m-d H:i:s'))) {
                        if ($etat->getLibelle() == "Ouverte" || $etat->getLibelle() == "Créée") {
                            array_push($restrictedEtatList, $etat);
                        }
                    }
                    if (strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s')) > strtotime(date('Y-m-d H:i:s')) && strtotime($sortie->getDateCloture()->format('Y-m-d H:i:s')) < strtotime(date('Y-m-d H:i:s'))) {
                        if ($etat->getLibelle() == "Clôturée") {
                            array_push($restrictedEtatList, $etat);
                            break;
                        }
                    }
                    if (strtotime($sortie->getDateDebut()->format('Y-m-d H:i:s')) == strtotime(date('Y-m-d H:i:s'))) {
                        if ($etat->getLibelle() == "Activé en cours") {
                            array_push($restrictedEtatList, $etat);
                            break;
                        }
                    }
                }
            }
            $sortie->setNoEtat($faker->randomElement($restrictedEtatList));
            $manager->persist($sortie);
        }
        $manager->flush();
        $allSorties = $manager->getRepository(Sortie::class)->findAll();
        foreach ($allUser as $user) {
            $specificListOfSortie = $allSorties;
            $nbOfSortieOfUser = $faker->numberBetween($min = 10, $max = 15);
            for ($i = 0; $i < $nbOfSortieOfUser; $i++) {
                $sortie = $faker->randomElement($specificListOfSortie);
                array_splice($specificListOfSortie, array_search($sortie, $specificListOfSortie), 1);
                $inscription = new Inscription();
                $inscription->setNoParticipant($user);
                $inscription->setDateInscription($faker->dateTimeBetween($startDate = '-3 months', $endDate = '+3 months', $timezone = null));
                $inscription->setNoSortie($sortie);
                $manager->persist($inscription);
            }
        }
        $manager->flush();
    }

    private function createSpecificuUser($allCampus, $faker, $manager)
    {
        $user = new User();
        $user->setUsername("ClementLemay");
        $user->setNom("Lemay");
        $user->setPrenom("Clément");
        $user->setTelephone("0750354911");
        $user->setEmail("zohar44@gmail.com");
        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$ZEx0a3pGdklKYTdNak40TQ$Xxf5Zec4Du+oDnuG17SgZgnaACUV/KTtIRmDZKoT7ro');
        $user->setIsAdministrateur(true);
        $user->setIsActif(true);
        $user->setNoCampus($faker->randomElement($allCampus));
        $user->setUrlPhoto(null);
        $user->setRoles(array("ROLE_ADMIN"));
        $manager->persist($user);
    }
}