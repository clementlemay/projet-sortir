<?php
namespace App\Command;
use App\Entity\Campus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use App\Services\ConvertCsvToArray;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportCsvUserCommand extends Command
{
    protected function configure()
    {
        // Name and description for app/console command
        $this
            ->setName('import:csv')
            ->addArgument('filename', InputArgument::REQUIRED, 'CSV File path')
            ->setDescription('Import users from CSV file');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Showing when the script is launched
        $now = new \DateTime();
        $output->writeln('<comment>Start : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');

        // Importing CSV on DB via Doctrine ORM
        $this->import($input, $output);

        // Showing when the script is over
        $now = new \DateTime();
        $output->writeln('<comment>End : ' . $now->format('d-m-Y G:i:s') . ' ---</comment>');
        return 1;
    }

    protected function import(InputInterface $input, OutputInterface $output)
    {
        // Getting php array of data from CSV
        $data = $this->get($input, $output);

        $kernel = $this->getApplication()->getKernel();
        $container = $kernel->getContainer();
        $em = $container->get('doctrine')->getManager();

        // Turning off doctrine default logs queries for saving memory
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        // Define the size of record, the frequency for persisting the data and the current index of records
        $size = count($data);
        $batchSize = 20;
        $i = 1;

        // Starting progress
        $progress = new ProgressBar($output, $size);
        $progress->start();

        // Processing on each row of data
        foreach($data as $row) {
            $user = $em->getRepository(User::class)
                ->findOneByEmail($row['email']);

            // If the user doest not exist we create one
            if(!is_object($user)){
                $user = new User();
                $user->setEmail($row['email']);
            }

            // Updating info

            // Récupération du campus
            $campus = $em->getRepository(Campus::class)
                ->find($row['no_campus']);
            $user->setNoCampus($campus);

            $user->setNom($row['nom']);
            $user->setPrenom($row['prenom']);
            $user->setTelephone($row['telephone']);
            $user->setRoles(explode(" ", $row['roles']));

            // Gestion de l'encryptage du password
            $passwordEncoder = $container->get('security.password_encoder');
            $user->setPassword($passwordEncoder->encodePassword($user, $row['password']));

            $user->setUsername($row['username']);
            $user->setIsAdministrateur($row['is_administrateur']);
            $user->setIsActif($row['is_actif']);

            // Do stuff here !

            // Persisting the current user
            $em->persist($user);

            // Each 20 users persisted we flush everything
            if (($i % $batchSize) === 0) {
                $em->flush();
                // Detaches all objects from Doctrine for memory save
                $em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);

                $now = new \DateTime();
                $output->writeln(' of users imported ... | ' . $now->format('d-m-Y G:i:s'));
            }
            $i++;
        }

        // Flushing and clear data on queue
        $em->flush();
        $em->clear();

        // Ending the progress bar process
        $progress->finish();
    }
    protected function get(InputInterface $input, OutputInterface $output)
    {
        // Getting the CSV from filesystem
        $fileName = $input->getArgument('filename');

        // Using service for converting CSV to PHP Array
        $converter = new ConvertCsvToArray();
        $data = $converter->convert($fileName, ';');

        return $data;
    }

}