<?php

namespace App\Form;

use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Sortie;
use App\Entity\Ville;
use App\Repository\EtatRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class  SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom de la sortie : '
            ])
            ->add('date_debut', DateTimeType::class, [
                'attr' => ['min' => ( new \DateTime() )->format('Y-m-d H:i')],
                'label' => 'Date et heure de la sortie : ',
                'choice_translation_domain' => true,
                'input' => 'datetime',
                'date_widget' => 'single_text',
            ])
            ->add('date_cloture', DateType::class, [
                'label' => 'Date limite des inscriptions : ',
                'widget' => 'single_text',
                'choice_translation_domain' => true
            ])
            ->add('nb_inscription_max', NumberType::class, [
                'label' => 'Nombre de places : '
            ])
            ->add('duree', NumberType::class, [
                'label' => 'Durée : '
            ])
            ->add('description_info', TextareaType::class, [
                'label' => 'Description et infos : '
            ])
            ->add('noCampus', EntityType::class, [
                'class' => Campus::class,
                'label' => 'Campus : ',
                'choice_label' => 'nomCampus'
            ])
            ->add('noVille', EntityType::class, [
                'class' => Ville::class,
                'label' => 'Ville : ',
                'choice_label' => 'nomVille',
                'mapped' => false
            ])
            ->add('noLieu', EntityType::class, [
                'class' => Lieu::class,
                'label' => 'Lieu : ',
                'choice_label' => 'nomLieu'
            ])
            ->add('noEtat', EntityType::class, [
                'class' => Etat::class,
                'label' => 'État : ',
                'choice_label' => 'libelle',
                'choices' => $this->getEtats(),
            ])
            ->add('url_photo', FileType::class, [
                'label' => 'Photo : ',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new Image([
                        'maxSize' => '10000k',
                        'maxSizeMessage' => 'Fichier trop volumineux'
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }

    protected function getEtats(){

        global $kernel;

        if ( 'AppCache' == get_class($kernel) )
        {
            $kernel = $kernel->getKernel();
        }

        $doctrine = $kernel->getContainer()->get( 'doctrine' );

        $etats = $doctrine->getRepository(Etat::class)->findBy([], [],2);

        return $etats;
    }
}
