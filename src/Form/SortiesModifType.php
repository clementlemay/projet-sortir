<?php

namespace App\Form;

use App\Entity\Campus;
use App\Entity\Sortie;
use App\Entity\Ville;
use App\Entity\Lieu;
use App\Entity\User;
use App\Entity\Etat;
use Proxies\__CG__\App\Entity\Lieu as EntityLieu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SortiesModifType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', null, [
                "label" => "Nom de la sortie : ",
                'required' => true])
            ->add('dateDebut', DateTimeType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => true,
                'label' => 'Date et heure de la sortie :'])
            ->add('duree', null, [
                'required' => true,
                "label" => "Durée : "])
            ->add('dateCloture', DateTimeType::class, [
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => true,
                'label' => 'Date limite d\'inscription :'])
            ->add('nbInscriptionMax', null, [
                'required' => true,
                "label" => "Nombre de place : "])
            ->add('descriptionInfo', TextareaType::class, [
                'required' => true,
                "label" => "Description : "])
            ->add('noCampus',EntityType::class,[
                'required' => true,
                'class' => Campus::class,
                'choice_label' => 'nomCampus',
                'choice_value' => 'noCampus',
                'label'=>'Ville organisatrice :',])
            ->add('noVille', EntityType::class, [
                'required' => true,
                'label' => 'Ville :',
                'mapped' => false,
                'class' => Ville::class,
                'choice_label' => 'nomVille',
                'choice_value' => 'noVille',])

            ->add('noLieu', EntityType::class,[
                'class'=>Lieu::class,
                'required' => true,
                'choice_label' => 'noLieu',
                'choice_value' => 'nomLieu',
                'label' => 'Lieu :',])

            ->add('rue', null, [
                'required' => true,
                'mapped' => false,
                "label" => "rue : "])
            ->add('codePostal', null, [
                'required' => true,
                'mapped' => false,
                "label" => "Code postal : "])
            ->add('latitude', null, [
                'required' => true,
                'mapped' => false,
                "label" => "Latitude : "])
            ->add('longitude', null, [
                'required' => true,
                'mapped' => false,
                "label" => "Longitude : "])
            ->add('organisateur', EntityType::class, [
                'class'=>User::class,
                'choice_label' => 'id',
                'choice_value' => 'nom',
                ])
            ->add('noEtat', EntityType::class, [
                'class' => Etat::class,
                'choice_label' => 'noEtat',
                'choice_value' => 'libelle',]);
   }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
            'data_class' => Sortie::class,
        ]);
    }
}
