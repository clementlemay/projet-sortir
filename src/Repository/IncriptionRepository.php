<?php

namespace App\Repository;

use App\Entity\Incription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Incription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Incription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Incription[]    findAll()
 * @method Incription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Incription::class);
    }

    // /**
    //  * @return Incription[] Returns an array of Incription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Incription
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
