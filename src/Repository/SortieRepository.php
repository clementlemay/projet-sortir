<?php

namespace App\Repository;

use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    public function listSortiesArchived()
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT s.no_sortie, s.nom as nomSortie, s.date_debut, s.date_cloture, s.no_sortie,
(SELECT COUNT(*) FROM inscription GROUP BY no_participant_id HAVING no_participant_id = s.no_sortie) as participants, s.nb_inscription_max as nbinscriptionsnmax,
(SELECT COUNT(inscription.no_id) FROM inscription WHERE inscription.no_sortie_id = s.no_sortie) AS Inscrit,
e.libelle, c.nom_campus, u.prenom, s.description_info, s.no_lieu_id
from sortie s
INNER JOIN etat e ON s.no_etat_id = e.no_etat
INNER JOIN user u ON s.organisateur_id = u.id
INNER JOIN campus c ON s.no_campus_id = c.no_campus
WHERE s.is_archived is false';


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function listeSortiesArchived()
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT s.no_sortie, s.nom as nomSortie, s.date_debut, s.date_cloture, s.no_sortie,
(SELECT COUNT(*) FROM inscription GROUP BY no_participant_id HAVING no_participant_id = s.no_sortie) as participants, s.nb_inscription_max as nbinscriptionsnmax,
(SELECT COUNT(inscription.no_id) FROM inscription WHERE inscription.no_sortie_id = s.no_sortie) AS Inscrit,
e.libelle, c.nom_campus, u.prenom, s.description_info, s.no_lieu_id
from sortie s
INNER JOIN etat e ON s.no_etat_id = e.no_etat
INNER JOIN user u ON s.organisateur_id = u.id
INNER JOIN campus c ON s.no_campus_id = c.no_campus
WHERE s.is_archived is true';


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function listSorties(int $idUser = null, \DateTime $datedebut = null, \DateTime $datefin = null, bool $mesSorties = false, bool $sortiesInscrites = false, bool $sortiesNonInscrites = false, bool $sortiesPassees = false)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT s.no_sortie, s.nom as nomSortie, s.date_debut, s.date_cloture, s.no_sortie,
(SELECT COUNT(*) FROM inscription GROUP BY no_participant_id HAVING no_participant_id = s.no_sortie) as participants, s.nb_inscription_max as nbinscriptionsnmax,
(SELECT COUNT(inscription.no_id) FROM inscription WHERE inscription.no_sortie_id = s.no_sortie) AS Inscrit,';
        if ($idUser != null) {
            $sql .= '(SELECT count(no_participant_id) FROM inscription WHERE inscription.no_sortie_id = s.no_sortie AND no_participant_id= ' . $idUser . ') as isInscrit,';
        }
        $sql .= 'e.libelle, c.nom_campus, u.prenom, s.description_info, s.no_lieu_id
from sortie s
INNER JOIN etat e ON s.no_etat_id = e.no_etat
INNER JOIN user u ON s.organisateur_id = u.id
INNER JOIN campus c ON s.no_campus_id = c.no_campus
WHERE s.is_archived is false';



        //dates renseignées
        if ($datedebut != null && $datefin != null) {
            $sql .= ' AND s.date_debut BETWEEN \'' . $datedebut->format('Y-m-d') . '\' AND \'' . $datefin->format('Y-m-d') . '\'';

            //mesorties
            if ($mesSorties == 1) {
                $sql .= ' AND s.organisateur_id =' . $idUser;
            }
            //sorties inscrites
            if ($sortiesInscrites == true) {
                $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 1';
            }
            //sorties non inscrites
            if ($sortiesNonInscrites == true) {
                $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 0';
            }
            //sorties passees
            if ($sortiesPassees == true) {
                $sql .= ' AND s.date_cloture < curdate()';
            }
        }
        // mes sorties + sorties non inscrites
        if ($mesSorties == true && $sortiesNonInscrites == true && $sortiesPassees == false && $datedebut == null && $datefin == null) {
            //echo " MesS + SNI ";
            $sql .= ' AND s.organisateur_id = ' . $idUser . ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 0';
        }
        // mes sorties + sorties inscrites
        if ($mesSorties == true && $sortiesInscrites == true && $sortiesPassees == false && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.organisateur_id = ' . $idUser . ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 1';
        }
        // mes sorties + sorties passées
        if ($mesSorties == true && $sortiesInscrites == false && $sortiesNonInscrites == false && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.organisateur_id = ' . $idUser . ' AND s.date_cloture < curdate()';
        }
        // mes sorties
        if ($mesSorties == true && $sortiesNonInscrites == false && $sortiesInscrites == false && $sortiesPassees == false && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.organisateur_id = ' . $idUser;
        }
        // juste sorties inscrites
        if ($sortiesInscrites == true && $mesSorties == false && $sortiesPassees == false && $datedebut == null && $datefin == null) {
            $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 1';
        }
        //juste sorties non inscrites
        if ($sortiesNonInscrites == true && $mesSorties == false && $sortiesPassees == false && $datedebut == null && $datefin == null) {
            $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 0';
        }
        //sorties passées + sorties inscrites
        if ($mesSorties == false && $sortiesInscrites == true && $sortiesNonInscrites == false && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 1 AND s.date_cloture < curdate()';
        }
        //Sorties passées + sorties pas inscrites
        if ($mesSorties == false && $sortiesInscrites == false && $sortiesNonInscrites == true && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 0 AND s.date_cloture < curdate()';
        }
        //Sorties passées seulement
        if ($mesSorties == false && $sortiesInscrites == false && $sortiesNonInscrites == false && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.date_cloture < curdate()';
        }
        //mesSorties + sorties où je suis inscrit + sorties passées
        if ($mesSorties == true && $sortiesInscrites == true && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.organisateur_id = ' . $idUser . ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 1 AND s.date_cloture < curdate()';
        }

        if ($mesSorties == true && $sortiesNonInscrites == true && $sortiesPassees == true && $datedebut == null && $datefin == null) {
            $sql .= ' AND s.organisateur_id = ' . $idUser . ' AND (SELECT COUNT(no_sortie_id) FROM inscription WHERE no_sortie_id = s.no_sortie AND no_participant_id = ' . $idUser . ') = 0 AND s.date_cloture < curdate()';
        }
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function listSortiesByLieu($noLieu): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = ('SELECT no_sortie as noSortie, s.nom as nomSortie, s.date_debut, s.date_cloture, s.no_sortie, s.nb_inscription_max as nbinscriptionsnmax,s.description_info,
                (SELECT COUNT(inscription.no_id) FROM inscription WHERE inscription.no_sortie_id = s.no_sortie) AS Inscrit
                from sortie s
                INNER JOIN etat e ON s.no_etat_id = e.no_etat
                INNER JOIN user u ON s.organisateur_id = u.id
                INNER JOIN campus c ON s.no_campus_id = c.no_campus
                INNER JOIN lieu l ON s.no_lieu_id = l.no_lieu
                WHERE s.is_archived is false
                AND l.no_lieu = ' . $noLieu);

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /*
    public function findOneBySomeField($value): ?Sortie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
